var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    ServiceModel = mongoose.model('serviceType');

    module.exports = function (app){
        app.use('/', router);
    };


router.post('/serviceDetails', function(req, res, next) {

    var serviceModel = new ServiceModel(req.body);
    serviceModel.save(function(err, result) {
        if (err){
            console.log('serviceDetailsPost failed: ' + err);
        }
        res.send(result);
    });
});


router.get('/allServiceDetails/:start/:range', function(req, res, next) {
    ServiceModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})



router.get('/allServiceDetailsDropDown', function(req, res, next) {
    ServiceModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })

})


router.get('/serviceBymongoId/:serviceMongoId',function(req,res,next){
        console.log('serviceMongoId', req.params.serviceMongoId);
        ServiceModel.findOne({"_id":req.params.serviceMongoId},function(err,result){
                      if(err)
                      {
                     console.log(err);
                      }
                      else
                      {
                         console.log(result);
                         res.send(result);

                      }
                      })

});


router.get('/serviceType/count', function(req, res, next) {
    ServiceModel.count(function(err,countData){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: countData};
        res.send(count);
    });
})

router.post('/editServiceBymongoId/:serviceId', function(req, res, next) {

        ServiceModel.findOneAndUpdate({"_id": req.params.serviceId},req.body,{upsert: true, new: true},
        function(err,result)
            {
                if(err){
                    console.log(err.stack)
                }
                else{
                    res.send(result)
                }

            });

})


router.delete('/serviceBymongoId/:serviceMongoId',function(req, res, next){

            ServiceModel.remove({"_id":req.params.serviceMongoId},function(err,result)
            {
            if(err)
            {
            console.log(err);
            }
            else
            {
            res.send(result)
            }

            });
            });




