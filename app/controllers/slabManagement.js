var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    SlabManagementModel = mongoose.model('slabManagementDetails');

    module.exports = function (app){
        app.use('/', router);
    };


router.post('/slabManagementDetails', function(req, res, next) {

    var slabManagementModel = new SlabManagementModel(req.body);
    slabManagementModel.save(function(err, result) {
        if (err){
            console.log('slabManagementDetails failed: ' + err);
        }
        res.send(result);
    });
});


router.get('/allSlabManagementDetails', function(req, res, next) {
    SlabManagementModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})


router.get('/SlabManagement/count', function(req, res, next) {
    SlabManagementModel.count(function(err,countData){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: countData};
        res.send(count);
    });
})



router.get('/slabManagementBymongoId/:slabManagementMongoId',function(req,res,next){

        SlabManagementModel.findOne({"_id":req.params.slabManagementMongoId},function(err,result){
                      if(err)
                      {
                     console.log(err);
                      }
                      else
                      {
                         console.log(result);
                         res.send(result);

                      }
                      })

});



router.post('/editSlabManagementBymongoId', function(req, res, next) {


        SlabManagementModel.findOneAndUpdate({"_id":req.body._id},req.body,{upsert: true, new: true},
        function(err,result)
            {
                if(err){
                    console.log(err.stack)
                }
                else{
                    res.send(result)
                }

            });

})


router.delete('/slabManagementBymongoId/:slabManagementMongoId',function(req, res, next){

            SlabManagementModel.remove({"_id":req.params.slabManagementMongoId},function(err,result)
            {
            if(err)
            {
            console.log(err);
            }
            else
            {
            res.send(result)
            }

            });
            });




