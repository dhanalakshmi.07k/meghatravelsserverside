var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    FuelModel = mongoose.model('fuelType');

    module.exports = function (app){
        app.use('/', router);
    };


router.post('/fuelDetails', function(req, res, next) {

    var fuelModel = new FuelModel(req.body);
    fuelModel.save(function(err, result) {
        if (err){
            console.log('fuelDetailsPost failed: ' + err);
        }
        res.send(result);
    });
});

router.get('/allFuelDetails/count', function(req, res, next) {


    FuelModel.count(function(err,author){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: author};
        res.send(count);
    });
})

router.get('/allFuelDetails/:start/:range', function(req, res, next) {
    FuelModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})


router.get('/allFuelsDropDown', function(req, res, next) {
    FuelModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })

})


router.get('/fuelBymongoId/:fuelMongoId',function(req,res,next){

        FuelModel.findOne({"_id":req.params.fuelMongoId},function(err,result){
                      if(err)
                      {
                     console.log(err);
                      }
                      else
                      {
                         console.log(result);
                         res.send(result);

                      }
                      })

});



router.post('/editFuelDetails/:fuelId', function(req, res, next) {

        FuelModel.findOneAndUpdate({"_id":req.params.fuelId},req.body,{upsert: true, new: true},
        function(err,result)
            {
                if(err){
                    console.log(err.stack)
                }
                else{
                    res.send(result)
                }

            });

})


router.delete('/fuelBymongoId/:fuelMongoId',function(req, res, next){

            FuelModel.remove({"_id":req.params.fuelMongoId},function(err,result)
            {
            if(err)
            {
            console.log(err);
            }
            else
            {
            res.send(result)
            }

            });
            });




