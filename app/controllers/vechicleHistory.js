var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    vechicleHistoryModel = mongoose.model('vechicleHistory');

module.exports = function (app){
    app.use('/', router);
};


router.post('/vechicleHistoryDetails', function(req, res, next) {

    var vechicleHistoryDetailsModel = new vechicleHistoryModel(req.body);
    vechicleHistoryDetailsModel.save(function(err, result) {
        if (err){
            console.log('shiftManagementDetails failed: ' + err);
        }
        res.send(result);
    });
});



router.get('/vechicleHistory/count', function(req, res, next) {
    vechicleHistoryModel.count(function(err,countData){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: countData};
        res.send(count);
    });
})

router.get('/allVechicleHistoryDetails/:skip/:limit', function(req, res, next) {
    vechicleHistoryModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})


router.get('/vechicleHistoryBymongoId/:vechicleHistoryMongoId',function(req,res,next){

    vechicleHistoryModel.findOne({"_id":req.params.vechicleHistoryMongoId},function(err,result){
        if(err)
        {
            console.log(err);
        }
        else
        {
            console.log(result);
            res.send(result);

        }
    })

});



router.post('/editvechicleHistoryBymongoId/:vechicleMongoId', function(req, res, next) {

    vechicleHistoryModel.findOneAndUpdate({"_id":req.params.vechicleMongoId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

})


router.delete('/vechicleHistoryBymongoId/:vechicleHistoryMongoId',function(req, res, next){

    vechicleHistoryModel.remove({"_id":req.params.vechicleHistoryMongoId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});




