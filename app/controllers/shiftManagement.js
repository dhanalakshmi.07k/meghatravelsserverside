var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    ShiftManagementModel = mongoose.model('shiftManagementDetails');

    module.exports = function (app){
        app.use('/', router);
    };


router.post('/shiftManagementDetails', function(req, res, next) {

    var shiftManagementModel = new ShiftManagementModel(req.body);
    shiftManagementModel.save(function(err, result) {
        if (err){
            console.log('shiftManagementDetails failed: ' + err);
        }
        res.send(result);
    });
});



router.get('/shiftManagement/count', function(req, res, next) {
    ShiftManagementModel.count(function(err,countData){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: countData};
        res.send(count);
    });
})

router.get('/allShiftManagementDetails', function(req, res, next) {
    ShiftManagementModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})


router.get('/shiftManagementBymongoId/:shiftManagementMongoId',function(req,res,next){

        ShiftManagementModel.findOne({"_id":req.params.shiftManagementMongoId},function(err,result){
                      if(err)
                      {
                     console.log(err);
                      }
                      else
                      {
                         console.log(result);
                         res.send(result);

                      }
                      })

});



router.post('/editShiftManagementBymongoId', function(req, res, next) {

        ShiftManagementModel.findOneAndUpdate({"_id":req.body._id},req.body,{upsert: true, new: true},
        function(err,result)
            {
                if(err){
                    console.log(err.stack)
                }
                else{
                    res.send(result)
                }

            });

})


router.delete('/shiftManagementBymongoId/:shiftManagementMongoId',function(req, res, next){

            ShiftManagementModel.remove({"_id":req.params.shiftManagementMongoId},function(err,result)
            {
            if(err)
            {
            console.log(err);
            }
            else
            {
            res.send(result)
            }

            });
            });




