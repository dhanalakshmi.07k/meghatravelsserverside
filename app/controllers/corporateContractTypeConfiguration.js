var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    corporateContractModel = mongoose.model('companyTranasactionDetails');

module.exports = function (app){
    app.use('/', router);
};


router.post('/corporateContractType', function(req, res, next) {

    var corporateModel = new corporateContractModel(req.body);
    corporateModel.save(function(err, result) {
        if (err){
            console.log('fuelDetailsPost failed: ' + err);
        }
        res.send(result);
    });
});

router.get('/corporateContractType/count', function(req, res, next) {
    corporateContractModel.count(function(err,author){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: author};
        res.send(count);
    });
})

router.get('/corporateContractType/:start/:range', function(req, res, next) {
    corporateContractModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})


router.get('/corporateContractModelById/:corporateContractMongoId',function(req,res,next){
    corporateContractModel.findOne({"_id":req.params.corporateContractMongoId},function(err,result){
        if(err)
        {
            console.log(err);
        }
        else
        {
            console.log(result);
            res.send(result);

        }
    })

});



router.post('/editcorporateContractModel/:corporateContractId', function(req, res, next) {
    corporateContractModel.findOneAndUpdate({"_id":req.params.corporateContractId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

})



router.delete('/corporateContractModelByiD/:corporateContractMongoId',function(req, res, next){
    corporateContractModel.remove({"_id":req.params.corporateContractMongoId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});




