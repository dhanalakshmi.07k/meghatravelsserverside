/**
 * Created by Suhas on 7/5/2016.
 */
let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let assetConfigurationInfoSchema =  new Schema({
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "assetConfiguration"});
let assetConfiguration = mongoose.model('assetConfiguration',assetConfigurationInfoSchema);
assetConfigurationInfoSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = assetConfiguration;