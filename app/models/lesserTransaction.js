/**
 * Created by Suhas on 7/5/2016.
 */
let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let lesserTransactionInfoSchema =  new Schema({
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "lesserTransactionDeatils"});
let lesserAssociationDetailsData = mongoose.model('lesserTransactionDeatils',lesserTransactionInfoSchema);
lesserTransactionInfoSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = lesserAssociationDetailsData;

