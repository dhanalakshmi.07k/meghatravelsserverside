/**
 * Created by Suhas on 7/5/2016.
 */
let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let lesserInfoSchema =  new Schema({
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "lesserAssociation"});
let lesserAssociationDetails = mongoose.model('lesserAssociation',lesserInfoSchema);
lesserInfoSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = lesserAssociationDetails;

