var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var vechicleTransactionSchema = new mongoose.Schema(
    {
        tripDate:String,
        CompanyId:String,
        vechicleNo:String,
        TSno:String,
        shiftType:String,
        vehicleMode:String,
        bookedBy:String,
        totalDay:Number,
        totalHrs:Number,
        totalKms:Number,
        extraHours:Number,
        extraKms:Number,
        ratePerHr:Number,
        ratePerKm:Number,
        slabRate:Number,
        extraAmtPerHr:Number,
        extraKmAmt:Number,
        tripAmt:Number,
        updated: { type: Date, default: Date.now },
        created: { type: Date, default: Date.now }

    },
    {collection:"vechicleTransactionDetails"});
mongoose.model('vechicleTransactionDetails',vechicleTransactionSchema);