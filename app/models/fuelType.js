var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var fuelTypeSchema = new mongoose.Schema(
    {
        fuelTypeName:String,
        fuelTypeDesp: String

    },
    {collection:"fuelType"});
mongoose.model('fuelType',fuelTypeSchema);